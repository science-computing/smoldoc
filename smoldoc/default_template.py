"""
Don't judge me for this.
"""

default_template = """
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{site_title}} : {{title}}</title>
    <style>
    :root {
    --color: #118bee;
    --color-accent: #118bee15;
    --color-bg: #fff;
    --color-bg-secondary: #e9e9e9;
    --color-secondary: #920de9;
    --color-text: #000;
    --color-text-secondary: #999;
    --font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif;
    --line-height: 1.5;
    --pad: 8px;
    --main-pad: 32px;
    --main-max: 800px;
}

@media (prefers-color-scheme: dark) {
    :root {
        --color: #0097fe;
        --color-accent: #0097fc4f;
        --color-bg: #333;
        --color-bg-secondary: #555;
        --color-secondary: #e20de9;
        --color-secondary-accent: #e20de94f;
        --color-shadow: #bbbbbb20;
        --color-text: #f7f7f7;
        --color-text-secondary: #aaa;
    }
}

body,html{
  margin: 0;
  padding: 0;
}

nav ul{
  padding: 0px 8px;
  list-style: none;
  font-size: 90%;
  text-indent: -4px;
  font-weight: 500;
}

a{
  color: var(--color-secondary);
}

nav li>ul{
  padding-left: 16px;
}

nav a{
  font-weight: normal;
  text-decoration: none;
}

body {
    background: var(--color-bg);
    color: var(--color-text);
    font-family: var(--font-family);
    line-height: var(--line-height);
    margin: 0;
}
div{
  box-sizing: border-box;
}
.container>*{
  padding: var(--pad);
}
.container {
  display: grid;

  grid-template-areas:
    "header header"
    "nav content";

  grid-template-columns: 200px 1fr;
  grid-template-rows: auto 1fr;
  grid-gap: 0px;

  height: 100vh;
}
header {
  grid-area: header;
  background-color: var(--color);
  color: var(--color-bg);
  padding: 8px;
}

nav {
  grid-area: nav;
  background-color: var(--color-bg-secondary);
  border-right: 4px solid var(--color-accent);
  padding: 12px;
}

main {
  grid-area: content;
  background-color: var(--color-bg);
}

.main-block{
  padding-left: var(--main-pad);
  padding-right: var(--main-pad);
  width: 100%;
  max-width: var(--main-max);
}

img{
  max-width: 100%;
}

@media (max-width: 768px) {
  .container {
    grid-template-areas:
      "header"
      "nav"
      "content";

    grid-template-columns: 1fr;
    grid-template-rows:
      auto /* Header */
      140px /* Nav */
      1fr /* Content */
  }

  
  nav{
    border-right: 0;
    overflow-y: scroll;
    border-bottom: 3px solid var(--color-accent);
  }
}
    </style>
    </head>
    <body>
      <div class="container">
        <header>
          {{site_title}}
        </header>
        <nav>
          <ul>
            <li><a href="{{site_url}}">Back to Site</a></li>
          </ul>
          {{sitemap|safe}}
        </nav>
        <main>
          <div class="main-block">
            {{body|safe}}
          </div>
        </main>
      </div>
    </body>
</html>
"""