# smoldoc
I've written like 100 static site generators at this point. This one's pretty small, and intended for general purpose documentation rendering for our python-based sites.

## Why did you build this, there's like 1000 other static site generators?
I wanted to use something that requires as few additional imports as possible. smalldoc only needs the `markdown` and (optionally) the `jinja2` package. 

## Use Case
If you have a *very* small set of user docs for your web application written in basic markdown, and are able to add a single command to your deploy pipeline, this is for you!


All smoldoc does is convert an input folder (with at least an index.md) to an output folder. `.md` files are converted to html, any other files are just copied over to the output directory.


Easy peasy


## Baseline Deployment
Install smoldoc
> For use with django / jinja2 see below

```pip install git+https://git.uwaterloo.ca/science-computing/smoldoc.git```

`cd` to any directory which has a `src` subdirectory filled with markdown files, and type `smoldoc`

smoldoc will create an 





## Using a configuration file
create the following directory structure:

```
config.json
src
 -> index.md
```

in config.json put:

```{"source_dir": "./src", "out_dir": "./out"}```

Then run smoldoc:

```smoldoc path/to/config.json```

the `out` directory will be created with index.html in it using the default internal template.

You can customize some other things by changing the values in your config.json. Here's the default:

```json
{
    "site_title": "smoldoc Documentation",
    "engine": "string",
    "source_dir": "./src",
    "out_dir": "./output",
    "site_url": "/",
    "template_file": null,
    "template_dirs": ["./templates"]
}
```

**Notes:**

- You only need to put the values you want to change in your own `config.json`. 
- `template_dirs` is only used for the `jinja` engine.
- All file paths in config.json must be relative to the config.json file -- the first thing smoldoc does is change the working directory to wherever config.json is!
- Files and folders starting with '_' will be skipped over. Use this for drafts if needed.

## Customizing the template
smoldoc doesn't make the prettiest default website. There's a little bit of CSS, but the entire template (without CSS) is this:

```html
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{site_title}} : {{title}}</title>
    <style></style>
  </head>
  <body>
    <div class="container">
      <header>
        {{site_title}}
      </header>
      <nav>
        <ul>
          <li><a href="{{site_url}}">Back to Site</a></li>
        </ul>
        {{sitemap|safe}}
      </nav>
      <main>
        <div class="main-block">
          {{body|safe}}
        </div>
      </main>
    </div>
  </body>
</html>
```

That uses 100% of the context variables offered by smoldoc. `body` and `sitemap` are pre-computed html, so they aren't changable. 

To replace this template, set the `template_file` variable in your `config.json` to a path relative to the config.

## Use with Jinja2
If you want to spice up your template a bit or are already using jinja2, add the following settings:

```json
{
    ...
    "template_dirs": ["path_to_templates"],
    "engine": "jinja"

}
```

Then you can easily set the `template_file` setting to anything within the template dirs.

## Use with Django

### The lazy way
Make a new app called user_docs with a static folder and compile everything into static, ensure you have an `__init__.py` in the folder and add it to your `INSTALLED_APPS`. Collectstatic will pick up your stuff on deploy.

### The less easy way
Same as above, but instead of making user_docs an app, just make it a normal folder and add your output directory to `STATIC_DIRS` so that collectstatic can find them.


### Using Django Template Engine
If you want to use the django rendering engine with a custom template to make loading static files / extend other templates easier, smoldoc can do this! First, set engine to:

```"engine": "django"```

and point your config to a template that django can find via the `template_file` setting.

Now run `DJANGO_SETTINGS_MODULE=yourapp.settings smoldoc /path/to/config.json` *in the project root directory*. Django template engine will now be used to render all your docs.

## Changing the order of sidebar links
- You can't

## Changing the HTML for the sidebar links
- Nope...