import os
from setuptools import find_packages, setup
with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()
# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))
setup(
    name='smoldoc',
    version='0.1',
    packages=find_packages(),
    scripts=['bin/smoldoc'],
    include_package_data=True,
    install_requires=[
        'markdown>=3.3,<3.4'
    ],
    license='None',
    description='Tiny Doc Generator requires only Markdown and either Django or Jinja',
    long_description=README,
    url='https://git.uwaterloo.ca/science-computing/smoldoc',
    author='Mirko Vucicevich',
    author_email='mvucicev@uwaterloo.ca',
    classifiers=[
        'Intended Audience :: Developers',
        'License :: Unlicense',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
)

